# SmartRep – Android Wear, Motion Sensors & Gestures

The primary aim of this project is to produce an Android application which will allow users to log a weightlifting
workout in combination with Android Wear functionalities, including motion sensors and heart rate monitors.
The application running on the Android Wear smartwatch will not be standalone. Hence, it will not function
without a Bluetooth connection to an Android mobile running the main application. 

The main objective of the Android Wear application will be automatic-repetition (auto-rep) functionality. The auto-rep functionality is
defined within this project as repetitions being tracked via an Android Wear device which the user is wearing.
Subsequently, this project will aim to provide the user with heart rate analysis. The heart rate analysis will allow
the user to view their heart rate for each exercise and provide insight into which exercises caused their heart rate
to reach its highest. This will be a feature for users which have Android Wear smartwatches with heart rate
monitors connected.