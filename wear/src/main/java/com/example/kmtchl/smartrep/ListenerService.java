package com.example.kmtchl.smartrep;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;


public class ListenerService extends WearableListenerService {
    private static final String MOBILE_DATA_PATH_ACCEL = "/mobile_data_accel";
    private static final String MOBILE_DATA_PATH_SAVEBUTTON = "/mobile_data_save";


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        DataMap dataMap;
        Log.v("SendDataLayerSpecs", "MOBILE_DATA_PATH");

        for (DataEvent event : dataEvents) {

            // Check the data type
            if (event.getType() == DataEvent.TYPE_CHANGED) {

                String path = event.getDataItem().getUri().getPath();
                if (path.equals(MOBILE_DATA_PATH_ACCEL)) {

                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

                    int minAccelChanges = dataMap.getInt("minAccelChanges"); // Convert datamap to int before putting in intent
                    int minAccelLevel = dataMap.getInt("minAccelLevel");

                    Log.v("ReceiveDataLayer", "DataMap received on wear: " + dataMap);

                    // Broadcast message to wearable activity for display
                    Intent dataIntent = new Intent();
                    dataIntent.setAction(Intent.ACTION_SEND);
                    dataIntent.putExtra("minAccelChanges", minAccelChanges);
                    dataIntent.putExtra("minAccelLevel", minAccelLevel);

                    LocalBroadcastManager.getInstance(this).sendBroadcast(dataIntent);

                }

            }
        }
    }
}
