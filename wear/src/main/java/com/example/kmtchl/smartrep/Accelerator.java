package com.example.kmtchl.smartrep;


import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;


public class Accelerator extends Activity implements SensorEventListener {
    private Activity activity;
    private int count;
    private int count2;
    private SensorManager manager;
    private Sensor accelerometer;
    private TextView reps;

    public Accelerator(Activity activity, TextView reps) {
        this.activity = activity;
        this.reps = reps;
        manager = (SensorManager) this.activity.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    //overriding two methods of SensorEventListener
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) { getAccelerometer(event); }
    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;

        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];
//        accel.setText(Float.toString(Math.abs(y)));

        if (y > 2) {
            count++;
            if (count%2 == 0) {
                reps.setText(Integer.toString(count2));
                count2++;
            }
        }
    }

    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and
        // accelerometer sensors
        manager.registerListener(this, manager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // unregister listener
        super.onPause();
        manager.unregisterListener(this);
    }
}
