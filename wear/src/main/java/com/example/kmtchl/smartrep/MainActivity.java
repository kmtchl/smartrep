package com.example.kmtchl.smartrep;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;


public class MainActivity extends Activity implements
        SensorEventListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private int positiveCount = 0;
    private int negativeCount = 0;

    private static final float SHAKE_THRESHOLD = 1.5f;
    private static final int SHAKE_WAIT_TIME_MS = 250;

    private String WEARABLE_DATA_PATH_REPS = "/wearable_data_reps";
    private String WEARABLE_DATA_PATH_HEARTBEAT = "/wearable_data_beat";

    private SensorManager sensorManager;
    private TextView reps, heartRate;
    private int count = 0, repetitions = 0, arrayCount = 0, minPositive, minNegative;
    private long theShakeTime = 0;
    private GoogleApiClient googleClient;

    private boolean shakeGestureOn;
    private Switch switchOnOff;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        reps = (TextView) findViewById(R.id.theReps);
        switchOnOff = (Switch) findViewById(R.id.switchOnOff);
        heartRate = (TextView) findViewById(R.id.theHeartRate);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        // Register the local broadcast receiver
        IntentFilter messageFilter = new IntentFilter(Intent.ACTION_SEND);
        MessageReceiver messageReceiver = new MessageReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, messageFilter);


        switchOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchOnOff.isChecked()) {
                    shakeGestureOn = true;
                    Toast.makeText(v.getContext(), "Auto-Rep enabled", Toast.LENGTH_SHORT).show();
                    repetitions = 0;
                    reps.setText(Integer.toString(repetitions) + " reps");
                } else {
                    shakeGestureOn = false;
                    Toast.makeText(v.getContext(), "Auto-Rep disabled", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        googleClient.connect();
    }

    //overriding two methods of SensorEventListener
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        detectShake(event);
        if (shakeGestureOn) { // if the shake gesture switches on
           if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                getAccelerometer(event);
           }
            if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
                getHeartRate(event);
            }
        }
        // if the shake gesture switches off
        else { } //
    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;

        // Movement
        float x = values[0];
        float y = Math.abs(values[1]);
        float z = Math.abs(values[2]);

        if (x > 2) {
            positiveCount++;
            Log.v("AccelChanges", "Positive changes: " + Integer.toString(positiveCount));
        }

        if (x < -2) {
            negativeCount++;
            Log.v("AccelChanges", "Negative changes: " + Integer.toString(negativeCount));
        }

        if (positiveCount > minPositive && negativeCount > minNegative) {

            repetitions++;

            DataMap dataMap = new DataMap();
            dataMap.putInt("repetitions", repetitions);
            new SendToDataLayerThread(WEARABLE_DATA_PATH_REPS, dataMap).start();

            reps.setText(Integer.toString(repetitions) + " reps");

            positiveCount = 0;
            negativeCount = 0;

        }

        // eliminate any small movements from counts
        if (Math.abs(x) < 0.01) {
            positiveCount = 0;
            negativeCount = 0;
        }

    }

    private void getHeartRate(SensorEvent event) {
        float[] values = event.values;
        float heartBeat = values[0];

        DataMap dataMap = new DataMap();
        dataMap.putFloat("heartRate", heartBeat);
        new SendToDataLayerThread(WEARABLE_DATA_PATH_HEARTBEAT, dataMap).start();

        heartRate.setText(String.format("%.0f", heartBeat)); 

    }

    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and accelerometer sensors
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_NORMAL);

        // register the heart rate listener
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // unregister listener
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Create a DataMap object and send it to the data layer
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Disconnect from the data layer when the Activity stops
    @Override
    protected void onStop() {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        super.onStop();
    }

    // Class allowing data objects to be sent to Data Layer
    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        // Constructor for sending data objects to the data layer
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        public void run() {
            // Construct a DataRequest and send over the data layer
            PutDataMapRequest putDMR = PutDataMapRequest.create(path);
            putDMR.getDataMap().putAll(dataMap);
            PutDataRequest request = putDMR.asPutDataRequest();
            DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();
            if (result.getStatus().isSuccess()) {
                Log.v("SendDataLayerReps", "DataMap: " + dataMap + " sent successfully to data layer ");
            }
            else {
                // Log an error
                Log.v("sendDataLayerReps", "ERROR: failed to send DataMap to data layer");
            }
        }
    }


    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            minPositive = intent.getIntExtra("minPositive", 15);
            minNegative = intent.getIntExtra("minNegative", 15);
        }
    }

    // Reference - https://github.com/drejkim/AndroidWearMotionSensors/blob/master/wear/src/main/java/com/drejkim/androidwearmotionsensors/SensorFragment.java
    private void detectShake(SensorEvent event) {
        long now = System.currentTimeMillis();

        if((now - theShakeTime) > SHAKE_WAIT_TIME_MS) {
            theShakeTime = now;

            if (event.values.length >1) {
                float gX = event.values[0] / SensorManager.GRAVITY_EARTH;
                float gY = event.values[1] / SensorManager.GRAVITY_EARTH;
                float gZ = event.values[2] / SensorManager.GRAVITY_EARTH;


                // gForce will be close to 1 when there is no movement
                float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

                // Do something if gForce exceeds threshold - shake gesture marked as on;
                // else, shake gesture marked as off
                if (gForce > SHAKE_THRESHOLD) {
                    if (shakeGestureOn) {
                        shakeGestureOn = false;
                        switchOnOff.setChecked(false);
                        Toast.makeText(this, "Auto-Rep disabled", Toast.LENGTH_SHORT).show();

                    } else {
                        shakeGestureOn = true;
                        switchOnOff.setChecked(true);
                        Toast.makeText(this, "Auto-Rep enabled", Toast.LENGTH_SHORT).show();
                        repetitions = 0;
                        reps.setText(Integer.toString(repetitions) + " reps");

                    }
                }
            }
        }
    }

}