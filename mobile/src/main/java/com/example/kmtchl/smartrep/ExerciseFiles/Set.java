package com.example.kmtchl.smartrep.ExerciseFiles;

/**
 * Created by kmtchl on 02/03/17.
 */

public class Set {
    private int weightLifted;
    private int repsCompleted;

    public Set(int weightLifted, int repsCompleted) {
        this.weightLifted = weightLifted;
        this.repsCompleted = repsCompleted;
    }

    public int getWeightLifted() {
        return weightLifted;
    }

    public void setWeightLifted(int weightLifted) {
        this.weightLifted = weightLifted;
    }

    public int getRepsCompleted() {
        return repsCompleted;
    }

    public void setRepsCompleted(int repsCompleted) {
        this.repsCompleted = repsCompleted;
    }
}
