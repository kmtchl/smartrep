package com.example.kmtchl.smartrep.Database;

/**
 * Created by kmtchl on 16/03/17.
 */

import android.provider.BaseColumns;

public final class FeedReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "WorkoutHistory";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_EXERCISE = "exercise";
        public static final String COLUMN_NAME_SETS = "sets";
        public static final String COLUMN_NAME_VOLUME = "volume";
        public static final String COLUMN_NAME_HEART_RATE = "heartRate";



    }


}