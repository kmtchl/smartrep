package com.example.kmtchl.smartrep.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kmtchl.smartrep.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kmtchl on 15/04/17.
 */

public class WorkoutHistoryAdapter extends RecyclerView.Adapter<WorkoutHistoryAdapter.MyViewHolder> {

    private LinkedHashMap<String, ArrayList<ArrayList<String>>> workoutHistoryHashMap = new LinkedHashMap<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView date, exercises, completedSets, totalVolume, avgHeartRate;


        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.date);
            exercises = (TextView) view.findViewById(R.id.previousExercises);
            completedSets = (TextView) view.findViewById(R.id.setsCompleted);
            totalVolume = (TextView) view.findViewById(R.id.textViewVolume);
            avgHeartRate = (TextView) view.findViewById(R.id.avgHeartRate);
        }
    }

    public WorkoutHistoryAdapter(LinkedHashMap<String, ArrayList<ArrayList<String>>> workoutHistoryHashMap) {
        this.workoutHistoryHashMap = workoutHistoryHashMap;
    }

    @Override
    public WorkoutHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workout_history_list_row, parent, false);
        return new WorkoutHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return workoutHistoryHashMap.size();
    }

    @Override
    public void onBindViewHolder(final WorkoutHistoryAdapter.MyViewHolder holder, int position) {
        String date;
        ArrayList<String> dates = new ArrayList<>();
        StringBuilder exerciseList = new StringBuilder();
        StringBuilder setsSB = new StringBuilder();
        int totalVolume = 0;
        float heartRate = 0;
        float averageHeartRate = 0;
        int heartRateCount = 0;


        // add all dates from hashmap to arraylist
        for (Map.Entry<String, ArrayList<ArrayList<String>>> entry : workoutHistoryHashMap.entrySet()) {
            dates.add(entry.getKey());
        }

        date = dates.get(position);

        // iterate through exercises within hashmap
        for (ArrayList<String> exercises : workoutHistoryHashMap.get(date)) {
            int sets = 0;

            exerciseList.append(exercises.get(0)).append(": \n");

            // iterate and get the amount of sets for each exercise
            sets = getSets(exercises, sets);

            // checking for plural vs singular
            if (sets < 1) {
                setsSB.append("No sets\n");
            } else if (sets < 2) {
                setsSB.append(sets).append(" set\n");
            } else {
                setsSB.append(sets).append(" sets\n");
            }


            // get the volume and add it to total volume
            totalVolume += Integer.parseInt(exercises.get(2));

            // get the heart rate
            if (! exercises.get(3).equals(0)) {
                heartRate += Float.parseFloat(exercises.get(3));
                heartRateCount++;
            }

        }

        holder.date.setText(dates.get(position));
        holder.exercises.setText(exerciseList);
        holder.completedSets.setText(setsSB);
        holder.avgHeartRate.setText("avgHeartRate");
        holder.totalVolume.setText("Total Volume: " + Integer.toString(totalVolume) + "kg");

        averageHeartRate = heartRate / heartRateCount;
        holder.avgHeartRate.setText(String.format("%.1f", averageHeartRate));

    }



    public int getSets(ArrayList<String> theString, int sets) {
        // iterate and list the amount of sets for each exercise
        for (int i = 0; i < theString.get(1).length() - 1; i++) {
            char character = theString.get(1).charAt(i);
            if (character == ',') {
                sets++;
            }
        }
        return sets;
    }



}