package com.example.kmtchl.smartrep;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kmtchl.smartrep.CategoryFiles.ChooseCategory;
import com.example.kmtchl.smartrep.WorkoutFiles.WorkoutHistory;

public class MobileMainActivity extends AppCompatActivity {
    private ImageView workoutNow, workoutHistory,calculateOneRM;
    private TextView workoutNowText, workoutHistoryText, calculateOneRMtext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mobile);

        workoutNow = (ImageView) findViewById(R.id.startWorkoutImage);
        workoutHistory = (ImageView) findViewById(R.id.workoutHistoryImage);
        calculateOneRM = (ImageView) findViewById(R.id.calculate1rmImage);

        workoutNowText = (TextView) findViewById(R.id.startWorkoutTextView);
        workoutHistoryText = (TextView) findViewById(R.id.workoutHistoryTextView);
        calculateOneRMtext = (TextView) findViewById(R.id.calculate1RMTextView);

        workoutNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, ChooseCategory.class);
                startActivity(intent);
            }
        });

        workoutNowText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, ChooseCategory.class);
                startActivity(intent);
            }
        });


        workoutHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, WorkoutHistory.class);
                startActivity(intent);
            }
        });

        workoutHistoryText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, WorkoutHistory.class);
                startActivity(intent);
            }
        });

        calculateOneRM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, Calculate1RM.class);
                startActivity(intent);
            }
        });

        calculateOneRMtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MobileMainActivity.this, Calculate1RM.class);
                startActivity(intent);
            }
        });


    }


}


