package com.example.kmtchl.smartrep.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.kmtchl.smartrep.ExerciseFiles.Exercise;
import com.example.kmtchl.smartrep.R;

import java.util.ArrayList;

/**
 * Created by kmtchl on 02/03/17.
 */


public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.MyViewHolder> {
    private ArrayList<Exercise> exerciseList, selectedExercise;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, musclesWorked;
        private CheckBox exerciseCheckBox;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            musclesWorked = (TextView) view.findViewById(R.id.musclesWorked);
            exerciseCheckBox = (CheckBox) view.findViewById(R.id.exerciseCheckBox);
        }
    }

    public ExerciseAdapter(ArrayList<Exercise> exerciseList, ArrayList<Exercise> selectedExercise, Context context) {
        this.exerciseList = exerciseList;
        this.selectedExercise = selectedExercise;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exercise_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Exercise exercise = exerciseList.get(position);

        // Check if exercise already selected on previous activity
        for (Exercise e : selectedExercise) {
            if (e.getName().equals(exercise.getName())) { holder.exerciseCheckBox.setChecked(true); }
        }

        if (exercise.getName().length() > 30) { holder.name.setTextSize(17); }

        holder.name.setText(exercise.getName());
        holder.musclesWorked.setText(exercise.getMusclesWorked().toString());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.exerciseCheckBox.toggle();
                addOrRemoveSelected(holder, exercise);

            }
        });

        holder.exerciseCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOrRemoveSelected(holder, exercise);
            }
        });
    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    // Add or remove exercise from Selected Exercises
    private void addOrRemoveSelected(MyViewHolder holder, Exercise exercise) {
        if (holder.exerciseCheckBox.isChecked()) {
            selectedExercise.add(exercise);
        }

        if (!holder.exerciseCheckBox.isChecked()) {
            selectedExercise.remove(exercise);
        }
    }



 }
