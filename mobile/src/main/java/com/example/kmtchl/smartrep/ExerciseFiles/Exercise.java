package com.example.kmtchl.smartrep.ExerciseFiles;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kmtchl on 01/03/17.
 */

public class Exercise implements Parcelable{
    private String name, equipmentRequired;
    private ArrayList musclesWorked;
    private HashMap personalBest;
    private boolean selected;
    private int minPositive, minNegative;
    private ArrayList<ArrayList<Double>> sets = new ArrayList<>();
    private int volume = 0;
    private float heartRate;


    public Exercise(String name, String equipmentRequired, ArrayList<String> musclesWorked, HashMap<Integer, Integer> personalBest, int minPositive, int minNegative) {
        this.name = name;
        this.equipmentRequired = equipmentRequired;
        this.musclesWorked = musclesWorked;
        this.personalBest = personalBest;
        selected = false;
        this.minPositive = minPositive;
        this.minNegative = minNegative;
    }

    public Exercise(String name, String equipmentRequired, ArrayList<String> musclesWorked, int minAccelLevel, int minAccelChanges, float heartRate) {
        this.name = name;
        this.equipmentRequired = equipmentRequired;
        this.musclesWorked = musclesWorked;
        selected = false;
        this.minPositive = getMinPositive();
        this.minNegative = getMinNegative();
        this.heartRate = heartRate;
    } // Constructor for when no personal best

    public Exercise(String name, String equipmentRequired, ArrayList<String> musclesWorked) {
        this.name = name;
        this.equipmentRequired = equipmentRequired;
        this.musclesWorked = musclesWorked;
        selected = false;
    } // Constructor for when no personal best & no accelerometer

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEquipmentRequired() {
        return equipmentRequired;
    }

    public void setEquipmentRequired(String equipmentRequired) {
        this.equipmentRequired = equipmentRequired;
    }

    public ArrayList<String> getMusclesWorked() {
        return musclesWorked;
    }

    public void setMusclesWorked(ArrayList<String> musclesWorked) {
        this.musclesWorked = musclesWorked;
    }

    public HashMap<Integer, Integer> getPersonalBest() {
        return personalBest;
    }

    public void setPersonalBest(int weight, int reps) {
        personalBest.put(weight, reps);
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean tf) {
        selected = tf;
    }


    public void setMinPositive(int minAccelLevel) {
        this.minPositive = minPositive;
    }

    public int getMinPositive() { return  minPositive; }

    public void setMinNegative(int minNegative) {
        this.minNegative = minNegative;
    }

    public int getMinNegative() { return  minNegative; }

    public void addSet(ArrayList<Double> weightReps) {
        sets.add(weightReps);
    }

    public ArrayList<ArrayList<Double>> getSets() {
        return sets;
    }

    public int getVolume() {
        for (ArrayList<Double> set : sets) {
            volume+= set.get(0) * set.get(1);
        }
        return volume;
    }

    public void setHeartRate(float heartRate) {
        this.heartRate = heartRate;
    }

    public float getHeartRate() {
        return heartRate;
    }


    private Exercise(Parcel in) {
        name = in.readString();
        equipmentRequired = in.readString();
        musclesWorked = in.readArrayList(ArrayList.class.getClassLoader());
        personalBest = in.readHashMap(HashMap.class.getClassLoader());
        selected = in.readInt() != 0;
        sets = in.readArrayList(ArrayList.class.getClassLoader());
        volume = in.readInt();
        minPositive = in.readInt();
        minNegative = in.readInt();
        heartRate = in.readFloat();
    }

    public static final Parcelable.Creator<Exercise> CREATOR = new Parcelable.Creator<Exercise>() {
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }

        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(equipmentRequired);
        dest.writeList(musclesWorked);
        dest.writeMap(personalBest);
        dest.writeInt(selected ? 1 : 0);
        dest.writeList(sets);
        dest.writeInt(volume);
        dest.writeInt(minPositive);
        dest.writeInt(minNegative);
        dest.writeFloat(heartRate);

    }
}
