package com.example.kmtchl.smartrep.WorkoutFiles;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.example.kmtchl.smartrep.Adapters.WorkoutHistoryAdapter;
import com.example.kmtchl.smartrep.Database.FeedReaderDbHelper;
import com.example.kmtchl.smartrep.MobileMainActivity;
import com.example.kmtchl.smartrep.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

public class WorkoutHistory extends AppCompatActivity {
    private ArrayList<ArrayList<String>> dbArrayList = new ArrayList<>();
    private ArrayList<String> cursorStrings = new ArrayList<>();

    private LinkedHashMap<String, ArrayList<ArrayList<String>>> hashMap3d = new LinkedHashMap<>();
    private ArrayList<ArrayList<String>> mainArrayList = new ArrayList<>();
    private ArrayList<String> exerciseArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        setContentView(R.layout.activity_workout_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FeedReaderDbHelper dbHelper = new FeedReaderDbHelper(this);

        // Add all database entries into 2d ArrayList
        addDBtoArray(dbHelper.getAllEntries());

        Log.v("DBArray", dbArrayList.toString());
        Log.v("HashmapAttempt", hashMap3d.toString()); // this is returning empty....


        Cursor cursor = dbHelper.getAllEntries();
        cursor.moveToFirst();


        Log.v("getAllEntries()", cursor.getString(0));


        WorkoutHistoryAdapter workoutHistoryAdapter = new WorkoutHistoryAdapter(hashMap3d);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.workout_history_recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(workoutHistoryAdapter);


    }

    public void addDBtoArray(Cursor cursor) {
        String prevDate = "";
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).format(new Date());


        cursor.moveToFirst();

        // check whether only 1 entry in database, i.e. the date is today's date
        if (cursor.getString(0).equals(date)) {
            exerciseArrayList = new ArrayList<>();
            exerciseArrayList.add(cursor.getString(1));
            exerciseArrayList.add(cursor.getString(2));
            exerciseArrayList.add(cursor.getString(3));
            exerciseArrayList.add(Float.toString(cursor.getFloat(4)));
            mainArrayList.add(exerciseArrayList);
            hashMap3d.put(date, mainArrayList);
        }

        while (cursor.moveToNext()) {
            // get the current date
            date = cursor.getString(0);

            if (!cursor.isFirst()) {
                // get the previous date
                cursor.moveToPrevious();
                prevDate = cursor.getString(0);
                cursor.moveToNext();
            }

            // if the previous date is the same as current, insert in to same array
            if (date.equals(prevDate)) {
                exerciseArrayList = new ArrayList<>();
                exerciseArrayList.add(cursor.getString(1));
                exerciseArrayList.add(cursor.getString(2));
                exerciseArrayList.add(cursor.getString(3));
                exerciseArrayList.add(Float.toString(cursor.getFloat(4)));
                mainArrayList.add(exerciseArrayList);

            }
            // otherwise, put array into hashmap & create new array
            else {
                hashMap3d.put(prevDate, mainArrayList);
                exerciseArrayList = new ArrayList<>();
                mainArrayList = new ArrayList<>();

                exerciseArrayList.add(cursor.getString(1));
                exerciseArrayList.add(cursor.getString(2));
                exerciseArrayList.add(cursor.getString(3));
                exerciseArrayList.add(Float.toString(cursor.getFloat(4)));
                mainArrayList.add(exerciseArrayList);
            }


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MobileMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}