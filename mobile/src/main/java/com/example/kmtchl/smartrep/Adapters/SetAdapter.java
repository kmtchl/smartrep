package com.example.kmtchl.smartrep.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kmtchl.smartrep.R;

import java.util.ArrayList;

/**
 * Created by kmtchl on 02/03/17.
 */


public class SetAdapter extends RecyclerView.Adapter<SetAdapter.MyViewHolder> {
    private Context context;
    private int selectedRow = -1;
    private ArrayList<ArrayList<Double>> allSets;
    private float heartRate;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView set, weight, reps, heartBeat;



        public MyViewHolder(View view) {
            super(view);
            set = (TextView) view.findViewById(R.id.setNumber);
            weight = (TextView) view.findViewById(R.id.setWeight);
            reps = (TextView) view.findViewById(R.id.setReps);
            heartBeat = (TextView) view.findViewById(R.id.heartBeatTextView);


        }
    }

    public SetAdapter() {
        allSets = new ArrayList<>();
    }


    public void addSets(ArrayList<ArrayList<Double>> allSets) {
        this.allSets.clear();
        this.allSets = new ArrayList<>(allSets);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.set_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList aSet = allSets.get(position);

        // highlight the selected row
//        if (selectedRow != -1) {
//            highlightRow(holder, selectedRow, position);
//        }

        // notify when selected row changes
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                notifyItemChanged(selectedRow);
//                selectedRow = position;
//                notifyItemChanged(selectedRow);
//            }
//        });


        String set = Integer.toString(position+1);
        String weight = aSet.get(0).toString();
        String reps  = aSet.get(1).toString();
        reps = reps.substring(0, reps.length()-2);  // remove decimals & point
        String hr = String.format("%.1f", aSet.get(2));

        holder.set.setText(set);
        holder.weight.setText( weight);
        holder.reps.setText(reps);
        holder.heartBeat.setText(hr);



    }

    @Override
    public int getItemCount() {
        return allSets.size(); }

    private void highlightRow(MyViewHolder holder, int selectedRow, int position) {
        if (selectedRow == position) {
            holder.itemView.setBackgroundColor(Color.LTGRAY);
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void unHighlightRow(MyViewHolder holder, int selectedRow) {
        holder.itemView.setBackgroundColor(Color.TRANSPARENT);
    }



}



