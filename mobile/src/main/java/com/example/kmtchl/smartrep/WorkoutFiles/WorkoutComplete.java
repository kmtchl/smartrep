package com.example.kmtchl.smartrep.WorkoutFiles;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kmtchl.smartrep.Database.FeedReaderContract;
import com.example.kmtchl.smartrep.Database.FeedReaderDbHelper;
import com.example.kmtchl.smartrep.MobileMainActivity;
import com.example.kmtchl.smartrep.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class WorkoutComplete extends AppCompatActivity {
    FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(this);
    private TextView exercises, totalSets, totalVolume, duration, mostIntenseText;
    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).format(new Date());
    int intSets = 0;
    int intTotalVolume = 0;
    long time;

    float mostIntenseHR;
    String mostIntenseExName;

    ArrayList<Float> allHeartRates = new ArrayList<>();
    Intent intent = new Intent();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_complete);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        time = getIntent().getLongExtra("duration", 0);


        exercises = (TextView) findViewById(R.id.textExercises);
        totalSets = (TextView) findViewById(R.id.textSets);
        totalVolume = (TextView) findViewById(R.id.textVolume);
        duration = (TextView) findViewById(R.id.textDuration);
        mostIntenseText = (TextView) findViewById(R.id.mostIntenseTextView);

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

    // Define a projection that specifies which columns from the database
    // you will actually use after this query.
        String[] projection = {
                //FeedReaderContract.FeedEntry._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME_DATE,
                FeedReaderContract.FeedEntry.COLUMN_NAME_EXERCISE,
                FeedReaderContract.FeedEntry.COLUMN_NAME_SETS,
                FeedReaderContract.FeedEntry.COLUMN_NAME_VOLUME,
                FeedReaderContract.FeedEntry.COLUMN_NAME_HEART_RATE
        };

    // Filter results WHERE "date" = "today's date"
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_DATE + " = ?";
        String[] selectionArgs = { date };

    // How you want the results sorted in the resulting Cursor
        String sortOrder =
                FeedReaderContract.FeedEntry.COLUMN_NAME_DATE + " DESC";

        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        while (cursor.moveToNext()) {
            String date = cursor.getString(0);
            String exercise = cursor.getString(1);
            String sets = cursor.getString(2);
            int volume = cursor.getInt(3);
            float heartRate = cursor.getFloat(4);


            intTotalVolume += volume;
            allHeartRates.add(heartRate);

//             find the most intense exercise
            for (int i = 1; i < allHeartRates.size(); i++) {
                if (i == 1) {
                    mostIntenseExName = exercise;
                    mostIntenseHR = heartRate;
                }

                if (allHeartRates.get(i) > allHeartRates.get(i-1)) {
                    mostIntenseExName = exercise;
                    mostIntenseHR = heartRate;
                }
            }

            if (cursor.isLast()) { exercises.append(" " + exercise); }
            else { exercises.append(" " + exercise + ",\n                   "); }

            Log.v("DatabaseCall1", "\nExercise: " + cursor.getString(1) + " \n\tSets: " + cursor.getString(2) + " \n\tVolume: " + cursor.getInt(3) + " \n\tDate: " + cursor.getString(0));
        }

        cursor.close();

        totalSets.setText(Integer.toString(intSets));
        totalVolume.setText(getString(R.string.total_volume) + " " + Integer.toString(intTotalVolume));
        duration.setText(getString(R.string.duration) + " " + Long.toString(time) + " minutes");
        mostIntenseText.setText("Most Intense Exercise: " + mostIntenseExName + " (" + Float.toString(mostIntenseHR) + " bpm)");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MobileMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }








}
