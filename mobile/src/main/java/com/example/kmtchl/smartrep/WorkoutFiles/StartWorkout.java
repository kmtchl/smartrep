package com.example.kmtchl.smartrep.WorkoutFiles;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kmtchl.smartrep.Adapters.SetAdapter;
import com.example.kmtchl.smartrep.Database.FeedReaderContract;
import com.example.kmtchl.smartrep.Database.FeedReaderDbHelper;
import com.example.kmtchl.smartrep.ExerciseFiles.Exercise;
import com.example.kmtchl.smartrep.MobileMainActivity;
import com.example.kmtchl.smartrep.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class StartWorkout extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private ArrayList<Exercise> selectedExercises = new ArrayList<>();

    private static final String MOBILE_DATA_PATH_ACCEL = "/mobile_data_accel";


    private GoogleApiClient googleClient;
    private RecyclerView recyclerView;
    private TextView exerciseName, setTitle, weightTitle, repsTitle, heartRateTitle;
    private EditText weightEdit, repsEdit;
    private Button buttonNext, buttonPrev, buttonIncreaseWeight, buttonDecreaseWeight, buttonIncreaseReps, buttonDecreaseReps, buttonSave, buttonClear;
    private int index = 0, arraySize;
    ArrayList<Float> heartRate = new ArrayList<>();
    private float avgHeartRate = 0;
    private SetAdapter setAdapter;
    long startTime = System.currentTimeMillis();
    long endTime;
    long duration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_workout);
        Bundle extras = getIntent().getExtras();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setAdapter = new SetAdapter();
        recyclerView = (RecyclerView) findViewById(R.id.completedSetsRecyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(setAdapter);

        exerciseName = (TextView) findViewById(R.id.startWorkoutExerciseName);
        weightEdit = (EditText) findViewById(R.id.inputWeight);
        repsEdit = (EditText) findViewById(R.id.inputReps);
        buttonIncreaseWeight = (Button) findViewById(R.id.btnIncreaseWeightOneRM);
        buttonDecreaseWeight = (Button) findViewById(R.id.btnDecreaseWeight);
        buttonIncreaseReps = (Button) findViewById(R.id.btnIncreaseRepsOneRM);
        buttonDecreaseReps = (Button) findViewById(R.id.btnDecreaseRepsOneRM);
        buttonSave = (Button) findViewById(R.id.btnSave);
        buttonClear = (Button) findViewById(R.id.btnClear);
        setTitle = (TextView) findViewById(R.id.setTitle);
        weightTitle = (TextView) findViewById(R.id.weightTitle);
        repsTitle = (TextView) findViewById(R.id.repsTitle);
        heartRateTitle = (TextView) findViewById(R.id.heartRateTitle);
        buttonNext = (Button) findViewById(R.id.btnNext);
        buttonPrev = (Button) findViewById(R.id.btnPrev);

        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (extras.containsKey("selectedExercises")) {
            selectedExercises = extras.getParcelableArrayList("selectedExercises");
        }
        if (extras.containsKey("index")) {
            index = extras.getInt("index");
        }

        if (selectedExercises != null) {
            exerciseName.setText(selectedExercises.get(index).getName());
            arraySize = selectedExercises.size()-1;

            if (selectedExercises.get(index).getEquipmentRequired().equals("Bodyweight")) {
                // disable these features as they will not be needed for bodyweight exercises
                weightEdit.setEnabled(false);
                buttonIncreaseWeight.setEnabled(false);
                buttonDecreaseWeight.setEnabled(false);

                Toast.makeText(StartWorkout.this, "Weight field not available for bodyweight exercises.", Toast.LENGTH_SHORT).show();

            }

            if (smartRepCheck(selectedExercises.get(index))) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.use_smartrep_msg)
                        .setTitle(R.string.use_smartrep);

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int minPositive, minNegative;

                        // Convert to SmartRep Exercise
                        createSmartRepEx(selectedExercises.get(index));

                        // Disable EditText field for reps
                        repsEdit.setKeyListener(null);
                        buttonIncreaseReps.setEnabled(false);
                        buttonDecreaseReps.setEnabled(false);

                        minPositive = selectedExercises.get(index).getMinPositive();
                        minNegative = selectedExercises.get(index).getMinNegative();

                        // Send minimum values to data layer for Wearable
                        DataMap dataMap = new DataMap();
                        dataMap.putInt("minPositive", minPositive);
                        dataMap.putInt("minNegative", minNegative);
                        dataMap.putLong("Time",System.currentTimeMillis());
                        new SendToDataLayerThread(MOBILE_DATA_PATH_ACCEL, dataMap).start();

                        // Register the local broadcast receiver - update repetitions
                        IntentFilter messageFilter = new IntentFilter(Intent.ACTION_SEND);
                        MessageReceiver messageReceiver = new MessageReceiver();
                        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(messageReceiver, messageFilter);

                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // DO NOTHING
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        }

        if (index == 0) {
            buttonPrev.setEnabled(false);
        }
        if (index == arraySize) {
            buttonNext.setText(R.string.complete);
        }


        buttonIncreaseWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentWeight = Double.parseDouble(weightEdit.getText().toString());
                currentWeight += 2.5;
                weightEdit.setText(Double.toString(currentWeight));
            }
        });

        buttonDecreaseWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentWeight = Double.parseDouble(weightEdit.getText().toString());
                if (currentWeight>=2.5) { currentWeight -= 2.5; }
                weightEdit.setText(Double.toString(currentWeight));
            }
        });

        buttonIncreaseReps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentReps = Double.parseDouble(repsEdit.getText().toString());
                currentReps++;
                repsEdit.setText(Integer.toString((int) currentReps));
            }
        });

        buttonDecreaseReps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentReps = Double.parseDouble(repsEdit.getText().toString());
                if (currentReps>0) { currentReps--; }
                repsEdit.setText(Integer.toString((int) currentReps));
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTitle.setText("Set");
                weightTitle.setText("Weight (kg)");
                repsTitle.setText("Reps");
                heartRateTitle.setText(getString(R.string.heartRateTitle));

                // get the average heart rate
                avgHeartRate = getHeartRate(heartRate);
                selectedExercises.get(index).setHeartRate(avgHeartRate);

                // add all relevant values into current set array
                ArrayList<Double> theSet = new ArrayList<>();
                theSet.add(Double.parseDouble(weightEdit.getText().toString()));
                theSet.add(Double.parseDouble(repsEdit.getText().toString()));
                theSet.add((double) avgHeartRate);

                // add set into the current exercise
                selectedExercises.get(index).addSet(theSet);

                // update the adapter
                setAdapter.addSets(selectedExercises.get(index).getSets());
                setAdapter.notifyDataSetChanged();

                // hide keyboard
                hideSoftKeyboard(StartWorkout.this);

                // reset values
                heartRate = new ArrayList<>();
                repsEdit.setText("0");


            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weightEdit.setText("0");
                repsEdit.setText("0");
            }
        });


        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (index == arraySize) {
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).format(new Date());
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    long durationMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);

                    // Database Variables
                    FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(getApplicationContext());
                    SQLiteDatabase db = mDbHelper.getWritableDatabase();
                    ContentValues values = new ContentValues();

                    for (Exercise e : selectedExercises) {

                        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_EXERCISE, e.getName());
                        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_SETS, e.getSets().toString());
                        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DATE, date);
                        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_VOLUME, e.getVolume());

                        // Check for NaN
                        if (e.getHeartRate() == Float.NaN) { e.setHeartRate((float) 0); }

                        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_HEART_RATE, e.getHeartRate());

                        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);

                    }
                    Intent intent = new Intent(StartWorkout.this, WorkoutComplete.class);
                    intent.putExtra("duration", durationMinutes);
                    startActivity(intent);
                }

                else {
                    index++;
                    Intent intent = new Intent(StartWorkout.this, StartWorkout.class);
                    Bundle extras = new Bundle();
                    extras.putInt("index", index);
                    extras.putParcelableArrayList("selectedExercises", selectedExercises);
                    intent.putExtras(extras);
                    startActivity(intent);
                }
            }
        });

        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                Intent intent = new Intent(StartWorkout.this, StartWorkout.class);
                Bundle extras = new Bundle();
                extras.putInt("index", index);
                extras.putParcelableArrayList("selectedExercises", selectedExercises);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        googleClient.connect();
    }

    private boolean smartRepCheck(Exercise e) {
        ArrayList<String> smartRepExercise = new ArrayList<>();
        boolean smartRep = false;
        smartRepExercise.add("Barbell Squat");
        smartRepExercise.add("Barbell Deadlift");
        smartRepExercise.add("Barbell Bench Press");

        for (String s : smartRepExercise) {
            if (s.equals(e.getName())) { smartRep = true; }
        }

        return smartRep;
    }

    private Exercise createSmartRepEx(Exercise e) {
        if (e.getName().equals("Barbell Squat")) {
            e.setMinPositive(12);
            e.setMinNegative(8);
        }

        if (e.getName().equals("Barbell Bench Press")) {
            e.setMinPositive(4);
            e.setMinNegative(3);
        }

        if (e.getName().equals("Barbell Deadlift")) {
            e.setMinPositive(16);
            e.setMinNegative(12);
        }
        return e;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Class allowing data objects to be sent to Data Layer
    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        // Constructor for sending data objects to the data layer
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        public void run() {
            // Construct a DataRequest and send over the data layer
            PutDataMapRequest putDMR = PutDataMapRequest.create(path);
            putDMR.getDataMap().putAll(dataMap);
            PutDataRequest request = putDMR.asPutDataRequest();
            DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();

            if (result.getStatus().isSuccess()) {
                Log.v("SendDataLayerSpecs", "DataMap: " + dataMap + " sent successfully to data layer ");
                Log.v("SendDataLayerSpecs", result.getDataItem().getUri().getPath().toString());
            }
            else {
                // Log an error
            Log.v("myTag", "ERROR: failed to send DataMap to data layer");
            }
        }
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int reps = intent.getIntExtra("repetitions", 0);
            float heartBeat = intent.getFloatExtra("heartRate", 0);

            // ensure heart beat isn't being updated when reps are sent
            if (heartBeat != 0 ) { heartRate.add(heartBeat); }

            // Display message in UI & check reps aren't being updated when heart beat sent
            if (reps != 0) { repsEdit.setText(Integer.toString(reps)); }

        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MobileMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public float getHeartRate(ArrayList<Float> heartBeats) {
        float totalHeartBeats = 0;
        float heartRate;

        for (float f : heartBeats) {
            totalHeartBeats += f;
        }

        heartRate = totalHeartBeats / heartBeats.size();
        return heartRate;
    }
}




