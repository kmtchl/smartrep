package com.example.kmtchl.smartrep.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kmtchl.smartrep.ExerciseFiles.Exercise;
import com.example.kmtchl.smartrep.R;
import com.example.kmtchl.smartrep.WorkoutFiles.ChooseExercise;

import java.util.ArrayList;

/**
 * Created by kmtchl on 02/03/17.
 */


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {
    private ArrayList<String> categoryList;
    private ArrayList<Exercise> exerciseList, selectedExercises;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView category;

        public MyViewHolder(View view) {
            super(view);
            category = (TextView) view.findViewById(R.id.category);

        }
    }

    public CategoryAdapter(ArrayList<String> categoryList) {
        this.categoryList = categoryList;
    }

    public CategoryAdapter(ArrayList<String> categoryList, ArrayList<Exercise> exerciseList, ArrayList<Exercise> selectedExercises) {
        this.categoryList = categoryList;
        this.exerciseList = exerciseList;
        this.selectedExercises = selectedExercises;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final String c = categoryList.get(position);
        final Context context = holder.itemView.getContext();
        holder.category.setText(c);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send intent with category clicked
                Intent intent = new Intent(context, ChooseExercise.class);
                Bundle bundle = new Bundle();
                bundle.putString("category", c);

                if (exerciseList != null) { bundle.putParcelableArrayList("exerciseArray", exerciseList); }
                if (selectedExercises != null) { bundle.putParcelableArrayList("selectedExercises", selectedExercises); }

                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


}



