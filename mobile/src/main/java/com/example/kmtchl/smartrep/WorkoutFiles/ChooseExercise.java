package com.example.kmtchl.smartrep.WorkoutFiles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.kmtchl.smartrep.Adapters.ExerciseAdapter;
import com.example.kmtchl.smartrep.CategoryFiles.ChooseCategory;
import com.example.kmtchl.smartrep.ExerciseFiles.Exercise;
import com.example.kmtchl.smartrep.R;

import java.util.ArrayList;

public class ChooseExercise extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ExerciseAdapter exerciseAdapter;
    String category;
    private ArrayList<Exercise> exerciseList = new ArrayList<>();
    private ArrayList<Exercise> selectedExercises = new ArrayList<>();
    private ArrayList<Exercise> categoryExercise = new ArrayList<>();
    private Button backButton, startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_exercise);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());



        backButton = (Button) findViewById(R.id.buttonBackChooseEx);
        startButton = (Button) findViewById(R.id.buttonStartChooseEx);

        Bundle bundle = getIntent().getExtras();
        category = bundle.getString("category");

        if (bundle.containsKey("exerciseArray")) {
            exerciseList = bundle.getParcelableArrayList("exerciseArray");
        }
        if (bundle.containsKey("selectedExercises")) {
            selectedExercises = bundle.getParcelableArrayList("selectedExercises");
        }

        checkExerciseList();

        exerciseAdapter = new ExerciseAdapter(categoryExercise, selectedExercises, this);
        recyclerView = (RecyclerView) findViewById(R.id.exerciseRecyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(exerciseAdapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseExercise.this, ChooseCategory.class);
                Bundle extras = new Bundle();
                extras.putParcelableArrayList("exerciseArray", exerciseList);
                extras.putParcelableArrayList("selectedExercises", selectedExercises);
                intent.putExtras(extras);
                startActivity(intent);

            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedExercises = getSelectedExercises(exerciseList);
                Intent intent = new Intent(ChooseExercise.this, StartWorkout.class);
                Bundle extras = new Bundle();
                extras.putParcelableArrayList("selectedExercises", selectedExercises);
                intent.putExtras(extras);
                startActivity(intent);

            }
        });
    }

    private void prepareExerciseData() {
        ArrayList<String> musclesWorked;

        // Barbell Bench Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        musclesWorked.add("Shoulders");
        musclesWorked.add("Triceps");
        Exercise exercise = new Exercise("Barbell Bench Press", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Calf Raises
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Calf Raises", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Close Grip Bench Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Triceps");
        exercise = new Exercise("Barbell Close Grip Bench Press", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Barbell Curl", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Deadlift
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Deadlift", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Decline Bench Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        exercise = new Exercise("Barbell Decline Press", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Front Squat
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Front Squat", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Incline Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        musclesWorked.add("Shoulders");
        musclesWorked.add("Triceps");
        exercise = new Exercise("Barbell Incline Press", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Lunge
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Lunge", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Row
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        exercise = new Exercise("Barbell Row", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Shoulder Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Shoulders");
        musclesWorked.add("Triceps");
        exercise = new Exercise("Barbell Shoulder Press", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Skullcrusher
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Barbell Skullcrusher", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Barbell Squat
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Squat", "Barbell", musclesWorked);
        exercise.setMusclesWorked(musclesWorked);
        exerciseList.add(exercise);

        // Barbell Sumo Deadlift
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        musclesWorked.add("Legs");
        exercise = new Exercise("Barbell Sumo Deadlift", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Cable Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Cable Curl", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Cable Horizontal Row
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        exercise = new Exercise("Cable Horizontal Row", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Cable Pulldown
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        exercise = new Exercise("Cable Pulldown", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Chin-up
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        musclesWorked.add("Biceps");
        exercise = new Exercise("Chin-up", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Concentration Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Concentration Curl", "Dumbbell",  musclesWorked);
        exerciseList.add(exercise);

        // Dips
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        musclesWorked.add("Shoulders");
        musclesWorked.add("Triceps");
        exercise = new Exercise("Dips", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Single Arm Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Dumbbell Single Arm Curl", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Single Arm Shoulder Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Shoulders");
        exercise = new Exercise("Dumbbell Single Arm Shoulder Press", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Bench Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        exercise = new Exercise("Dumbbell Bench Press", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Calf Raise
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Dumbbell Calf Raise", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Biceps");
        exercise = new Exercise("Dumbbell Curl", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Fly
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        exercise = new Exercise("Dumbbell Fly", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Lateral Raise
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Shoulders");
        exercise = new Exercise("Dumbbell Lateral Raise", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Rear Delt Row
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Shoulders");
        exercise = new Exercise("Dumbbell Rear Delt Row", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Row
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        exercise = new Exercise("Dumbbell Row", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Shoulder Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Shoulders");
        exercise = new Exercise("Dumbbell Shoulder Press", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Skullcrusher
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Triceps");
        exercise = new Exercise("Dumbbell Skullcrusher", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Stiff Legged Deadlift
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Dumbbell Stiff Legged Deadlift", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Dumbbell Tricep Extension
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Triceps");
        exercise = new Exercise("Dumbbell Tricep Extension", "Dumbbell", musclesWorked);
        exerciseList.add(exercise);

        // Hack Squat
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Hack Squat", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Leg Curl
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Leg Curl", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Leg Extension
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Leg Extension", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Leg Press
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Leg Press", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Leg Raises
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Abs");
        exercise = new Exercise("Leg Raises", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Pull-up
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        musclesWorked.add("Biceps");
        exercise = new Exercise("Pull-up", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Plank
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Abs");
        exercise = new Exercise("Plank", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Push-up
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Chest");
        musclesWorked.add("Shoulders");
        musclesWorked.add("Triceps");
        exercise = new Exercise("Push-up", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Tricep Pushdown
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Triceps");
        exercise = new Exercise("Tricep Pushdown", "Machine", musclesWorked);
        exerciseList.add(exercise);

        // Seal Row
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Back");
        exercise = new Exercise("Seal Row", "Barbell", musclesWorked);
        exerciseList.add(exercise);

        // Sit Up
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Abs");
        exercise = new Exercise("Sit-up", "Bodyweight", musclesWorked);
        exerciseList.add(exercise);

        // Stiff Legged Deadlift
        musclesWorked = new ArrayList<>();
        musclesWorked.add("Legs");
        exercise = new Exercise("Stiff Legged Deadlift", "Barbell", musclesWorked);
        exerciseList.add(exercise);

    }

    private void checkExerciseList() {
        if (exerciseList.isEmpty()) {
            prepareExerciseData();
        }
        for (Exercise e : exerciseList) {
            ArrayList<String> musclesWorked = e.getMusclesWorked();
            for (String s : musclesWorked) {
                if (s.equals(category)) {
                    categoryExercise.add(e);
                }
            }
        }
    }

    public ArrayList<Exercise> getSelectedExercises(ArrayList<Exercise> exerciseList) {
        for (Exercise e : exerciseList) {
            if (e.getSelected()) {
                selectedExercises.add(e);
            }
        }
        return selectedExercises;
    }



}
