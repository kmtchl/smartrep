package com.example.kmtchl.smartrep;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

public class ListenerService extends WearableListenerService {

    private static final String WEARABLE_DATA_PATH_REPS = "/wearable_data_reps";
    private static final String WEARABLE_DATA_PATH_HEARTBEAT = "/wearable_data_beat";


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        DataMap dataMap;
        for (DataEvent event : dataEvents) {

            // Check the data type
            if (event.getType() == DataEvent.TYPE_CHANGED) {

                // Check the data path
                String path = event.getDataItem().getUri().getPath();

                if (path.equals(WEARABLE_DATA_PATH_REPS)) {
                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

                    int dm = dataMap.getInt("repetitions"); // Convert datamap to int before putting in intent
                    Log.v("ReceiveDataLayer", "DataMap received on mobile (repetitions): " + dataMap);

                    // Broadcast message
                    Intent dataIntent = new Intent();
                    dataIntent.setAction(Intent.ACTION_SEND);
                    dataIntent.putExtra("repetitions", dm);

                    LocalBroadcastManager.getInstance(this).sendBroadcast(dataIntent);
                }

                if (path.equals(WEARABLE_DATA_PATH_HEARTBEAT)) {
                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();

                    float hb = dataMap.getFloat("heartRate");
                    Log.v("ReceiveDataLayer", "DataMap received on mobile (heart rate): " + dataMap);

                    // Broadcast message
                    Intent dataIntent = new Intent();
                    dataIntent.setAction(Intent.ACTION_SEND);
                    dataIntent.putExtra("heartRate", hb);

                    LocalBroadcastManager.getInstance(this).sendBroadcast(dataIntent);

                }

            }
        }
    }
}