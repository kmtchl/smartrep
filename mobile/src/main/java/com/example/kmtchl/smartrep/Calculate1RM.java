package com.example.kmtchl.smartrep;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Calculate1RM extends AppCompatActivity {
    private Button increaseReps, decreaseReps, increaseWeight, decreaseWeight, calculate1RM;
    private EditText editTextWeight, editTextReps;
    private TextView textViewOneRM;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate1_rm);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        increaseReps = (Button) findViewById(R.id.btnIncreaseRepsOneRM);
        decreaseReps = (Button) findViewById(R.id.btnDecreaseRepsOneRM);
        increaseWeight = (Button) findViewById(R.id.btnIncreaseWeightOneRM);
        decreaseWeight = (Button) findViewById(R.id.btnDecreaseWeight);
        editTextWeight = (EditText) findViewById(R.id.inputWeight);
        editTextReps = (EditText) findViewById(R.id.inputReps);
        textViewOneRM = (TextView) findViewById(R.id.OneRMnumber);
        calculate1RM = (Button) findViewById(R.id.calculateOneRM);





        increaseWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentWeight = Double.parseDouble(editTextWeight.getText().toString());
                currentWeight += 2.5;
                editTextWeight.setText(Double.toString(currentWeight));
            }
        });

        decreaseWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentWeight = Double.parseDouble(editTextWeight.getText().toString());
                if (currentWeight>=2.5) { currentWeight -= 2.5; }
                editTextWeight.setText(Double.toString(currentWeight));
            }
        });

        increaseReps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentReps = Double.parseDouble(editTextReps.getText().toString());
                currentReps++;
                editTextReps.setText(Integer.toString((int)currentReps));
            }
        });

        decreaseReps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double currentReps = Double.parseDouble(editTextReps.getText().toString());
                if (currentReps>0) { currentReps--; }
                editTextReps.setText(Integer.toString((int)currentReps));
            }
        });


        calculate1RM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(Calculate1RM.this);
                double reps = Double.parseDouble(editTextReps.getText().toString());
                double weight = Double.parseDouble(editTextWeight.getText().toString());
                int oneRM = calculateOneRM(weight, reps);
                textViewOneRM.setText(Integer.toString(oneRM) + " kg");
            }
        });


    }

    private int calculateOneRM(double weight, double reps) {
        double oneRM;

        // Brzycki formula https://www.brianmac.co.uk/maxload.htm
        oneRM = weight / (1.0278 - (0.0278 * reps));

        return (int) (Math.round(oneRM));
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MobileMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
