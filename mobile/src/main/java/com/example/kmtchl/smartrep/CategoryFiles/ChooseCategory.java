package com.example.kmtchl.smartrep.CategoryFiles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.example.kmtchl.smartrep.Adapters.CategoryAdapter;
import com.example.kmtchl.smartrep.ExerciseFiles.Exercise;
import com.example.kmtchl.smartrep.MobileMainActivity;
import com.example.kmtchl.smartrep.R;

import java.util.ArrayList;

public class ChooseCategory extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter;
    private ArrayList<String> categoryList = new ArrayList<>();
    private ArrayList<Exercise> exerciseList, selectedExercises;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_category);

        recyclerView = (RecyclerView) findViewById(R.id.categoryRecyclerView);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.containsKey("exerciseArray")) {
                exerciseList = bundle.getParcelableArrayList("exerciseArray");
                for (Exercise e: exerciseList){
                    if (e.getSelected()) {
                        Log.v("ChooseCategory", e.getName());
                    }
                }
            }
            if (bundle.containsKey("selectedExercises")) {
                selectedExercises = bundle.getParcelableArrayList("selectedExercises");
            }
        }


        categoryAdapter = new CategoryAdapter(categoryList, exerciseList, selectedExercises);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(categoryAdapter);

        prepareCategoryData();
    }

    private void prepareCategoryData() {
        categoryList.add("Abs");
        categoryList.add("Back");
        categoryList.add("Biceps");
        categoryList.add("Chest");
        categoryList.add("Legs");
        categoryList.add("Shoulders");
        categoryList.add("Triceps");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MobileMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
